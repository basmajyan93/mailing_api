from celery import shared_task
from datetime import timedelta, datetime
from tzlocal import get_localzone
import requests
import api.models 


@shared_task
def send_notifications(id,text,operator_code,tag,end_time):
    if operator_code and tag:
        clients = api.models.Client.objects.filter(tag = tag, operator_code = operator_code)
    elif  not operator_code and tag:
        clients = api.models.Client.objects.filter(tag = tag)
    elif  operator_code and not tag:
        clients = api.models.Client.objects.filter(operator_code = operator_code)
    else:
        clients = api.models.Client.objects.all()    


    for client in clients:
        dt_now = datetime.now()
        end_time_date = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')
        if dt_now > end_time_date:    
            break
        send_notification(id,client.id,text)


@shared_task
def send_notification(id,client,text):
    eta_time = datetime.now() + timedelta(seconds=120)
    maling = api.models.Mailing.objects.get(id = id)
    client = api.models.Client.objects.get(id = client)
    message = api.models.Message.objects.create(
        status = 'sent',
        mailing = maling,
        client = client,
    )
    try:
        jwt_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTU5NTM0MTMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS92YWhlYmFzbWFqeWFuIn0.UzvhWY1lRRW81WeMBNUjDnRGJeC8_McYgWC4amXJR_A"
        headers = {
            "Authorization": f"Bearer {jwt_token}",
            "Content-Type": "application/json"
        }
        message_data = {
            "id": message.id,
            "phone": client.phone_number,
            "text": text
        }
        data = requests.post(f'https://probe.fbrq.cloud/v1/send/{message.id}',json = message_data,headers=headers,timeout = 30)      
        if data.status_code == 400:        
            api.models.Message.objects.filter(id=message.id).update(status = 'failed')
            send_notification.apply_async(eta = eta_time, args=[id,client,text])                
            
    except:
        api.models.Message.objects.filter(id=message.id).update(status = 'failed')
        send_notification.apply_async(eta = eta_time, args=[id,client,text])                
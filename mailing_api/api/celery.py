import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mailing_api.settings')

app = Celery('mailing_api')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

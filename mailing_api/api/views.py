from datetime import datetime , timedelta
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView , ListAPIView
from .serializers import * 
from .tasks import * 
from .models import * 

    
class CreateMailing(ListCreateAPIView):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

class UpdateMailing(APIView):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    def get(self,request, id):        
        try:
            serializer = MailingSerializer(Mailing.objects.get(id = id))
        except:
            return Response('Mailing not found', status = 404)
        return Response(serializer.data)
    
    def put(self,request, id):
        data = request.data
        start_time = data['start_time']
        end_time = data['end_time']
        text = data['text']
        operator_code = data['operator_code']
        tag = data['tag']
        try:
            Mailing.objects.filter(id = id).update(start_time  = start_time,end_time  = end_time,text  = text,operator_code  = operator_code,tag  = tag)            
        except:
            return Response('Mailing not found', status = 404)
        return Response(f'Mailing {id} is updated')
        
    def delete(self,request, id):
        try:
            Mailing.objects.get(id = id).delete()
        except:
            return Response('Mailing not found', status = 404)
        return Response(f'Mailing {id} is deleted')
    
    
class CreateClient(ListCreateAPIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class UpdateClient(APIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    def get(self,request, id):        
        try:
            serializer = ClientSerializer(Client.objects.get(id = id))
        except:
            return Response('Mailing not found', status = 404)
        return Response(serializer.data)
    
    def put(self,request, id):
        data = request.data
        operator_code = data['operator_code']
        tag = data['tag']
        timezone = data['timezone']
        try:
            Client.objects.filter(id = id).update(operator_code = operator_code,tag = tag,timezone = timezone)            
        except:
            return Response('Client not found', status = 404)
        return Response(f'Client {id} is updated')
        
    def delete(self,request, id):
        try:
            Client.objects.get(id = id).delete()
        except:
            return Response('Client not found', status = 404)
        return Response(f'Client {id} is deleted')
    

class MessageView(ListAPIView):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()


class StatisticView(APIView):
    def get(self,request):
        tags = list(set(list(map(lambda client : client.tag ,Client.objects.all()))))
        operator_code =list(set(list(map(lambda client : client.operator_code ,Client.objects.all()))))
        statistic = {
            'mailings':Mailing.objects.all().count(),
            'messages':Message.objects.all().count(),
            'success_messages':Message.objects.filter(status= 'sent').count(),
            'clients':Client.objects.all().count(),
            'tags': tags,
            'operator_code': operator_code,
        }
        return Response(statistic)



from django.urls import path 
from .views import *

urlpatterns = [   
    path('mailing', CreateMailing.as_view()),
    path('mailing/<str:id>', UpdateMailing.as_view()),
    path('client', CreateClient.as_view()),
    path('client/<str:id>', UpdateClient.as_view()),
    path('message', MessageView.as_view()),
    path('statistic', StatisticView.as_view()),
]

from django.db import models
from django.core.validators import RegexValidator
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime, timezone 
from .tasks import send_notifications
from tzlocal import get_localzone



class Mailing(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    text = models.TextField()
    operator_code = models.CharField(max_length=10, validators=[RegexValidator(r'^\d{1,10}$')],null=True,blank=True)
    tag = models.CharField(max_length=255,null=True,blank=True)

    def save(self, *args, **kwargs):
        if self.start_time > self.end_time:
            raise ValueError("start time cannot be later than end time")
        super().save(*args, **kwargs)


class Client(models.Model):
    phone_regex = RegexValidator(r'^7\d{10}$', "number must be in the format '7XXXXXXXXXX'.")
    phone_number = models.CharField(max_length=11, validators=[phone_regex], unique=True)
    operator_code = models.CharField(max_length=10, validators=[RegexValidator(r'^\d{1,10}$')])
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=255,null=True,blank=True)


class Message(models.Model):
    STATUS_CHOICES = [
        ('sent', 'Sent'),
        ('failed', 'Failed'),
    ]
    creation_time = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)



@receiver(post_save, sender=Mailing)
def start_mailing(sender, instance, created, **kwargs):
    if created:
        local_timezone = get_localzone()
        now = datetime.now(local_timezone).strftime('%Y-%m-%d %H:%M:%S')
        start_time = instance.start_time.strftime('%Y-%m-%d %H:%M:%S')
        end_time = instance.end_time.strftime('%Y-%m-%d %H:%M:%S')
        print(start_time < now < end_time)
        print(start_time , now , end_time)
        if start_time < now < end_time:
            print(1)
            send_notifications.delay(instance.id,instance.text,instance.operator_code,instance.tag,end_time)
        elif  now < start_time < end_time:
            print(2)
            send_notifications.apply_async(eta = start_time, args=[instance.id,instance.text,instance.operator_code,instance.tag,end_time])

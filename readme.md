Команда запуска: docker compose up


--Описание API--

Путь: /api/mailing

Метод: GET
Описание: Получение рассылки
Параметры запроса: нет    
Пример успешного ответа:
    Возвращает список рассылок
    
Метод: POST
Описание: Создание рассылки
Параметры запроса: 
    - start_time (string, обязательный) 
    - end_time (string, обязательный) 
    - text (string, обязательный) 
    - operator_code (int) 
    - tag (string) 
Пример успешного ответа:
    HTTP/1.1 201 Created
    Content-Type: application/json
    {    
      "id": "1",
      "start_time": 2023-05-22T10:00:00Z,
      "end_time": 2023-05-22T10:00:00Z,
      "text": "Hello world"
      "operator_code": "123"
      "tag": "Human"
    }

---

Путь: /api/mailing/{id}

Метод: PUT
Описание: Обновление рассылки
Параметры запроса:
    - start_time (string, обязательный) 
    - end_time (string, обязательный) 
    - text (string, обязательный) 
    - operator_code (int) 
    - tag (string) 
Пример успешного ответа:
    - Mailing 1 is updated

Метод: DELETE
Параметры запроса: нет    
Пример успешного ответа:
    - Mailing 1 is deleted
    
---

Путь: /api/client
Метод: GET
Описание: Получение клиента
Параметры запроса: Нет
Пример успешного ответа:
    - Список клиентов

Метод: POST
Описание: Создание клиента
Параметры запроса: 
    - phone_regex (int, обязательный) 
    - phone_number (int, обязательный) 
    - operator_code (int, обязательный) 
    - tag (string, обязательный) 
    - timezone (string) 
Пример успешного ответа:
    HTTP/1.1 201
    Content-Type: application/json
    {
      "id": "1",
      "phone_regex": 71231231231,
      "phone_number": 71231231231,
      "operator_code": "123"
      "tag": "cat"
      "timezone": "+3"
    }

---

Путь: /api/client/{id}

Метод: PUT
Описание: Обновление клиента
Параметры запроса:
    - operator_code (int, обязательный) 
    - tag (string, обязательный) 
    - timezone (string) 
Пример успешного ответа:
    HTTP/1.1 200 OK
    Content-Type: application/json
    {
      "id": "1",
      "phone_regex": 71231231231,
      "phone_number": 71231231231,
      "operator_code": "123"
      "tag": "cat"
      "timezone": "+3"
    }

Метод: DELETE
Параметры запроса: нет    
Пример успешного ответа:
    - Client 1 is deleted

---

Путь: /api/message

Метод: GET
Описание: Получение сообщений
Параметры запроса: Нет
Пример успешного ответа:
    HTTP/1.1 200 OK
    Content-Type: application/json
    [
      {
        "id": "1",
        "creation_time": "2023-05-22T10:00:00Z"
        "status": "sent",
        "mailing": "3",
        "client": "2"
      },
      {
        "id": "2",
        "creation_time": "2023-05-22T10:00:00Z"
        "status": "failed",
        "mailing": "1",
        "client": "3"
      }
    ]

---


Метод: GET
Путь: /api/statistic
Описание: Получение статистики
Параметры запроса: Нет
Пример успешного ответа:
    HTTP/1.1 200 OK
    Content-Type: application/json
  {
    "mailings": 10,
    "messages": 20,
    "success_messages": 18,
    "clients": 5,
    "tags": ['dog','cat','human'],
    "operator_code": [123,111]
}
